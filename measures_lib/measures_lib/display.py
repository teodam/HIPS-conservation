import math
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import matplotlib.gridspec as gridspec


def get_marker_color(avg):
    # Give a color to a point according to its value
    if avg < 0.2:
        # Good values. Not displayed
        return ('no')
    elif avg < 0.3:
        # Values a bit high. Plotted in green
        return ('go')
    else:
        # Value too high. Plotted in red
        return ('ro')


def plot(data_file_name):
    # Load the data file using only some columns
    tab = np.loadtxt(data_file_name, skiprows=2, usecols=(0, 1, 6, 7, 9, 14, 15, 17, 22, 23, 25), delimiter='\t')

    # left numbers represents the position in the original table of values
    # right numbers represents the position once we have extracted the columns with the loadtext function
    #
    # COORDS
    # 0, ra    0
    # 1, dec   1
    # FITS
    # 6, sum   2
    # 7, sig   3
    # 9, moy   4
    # HIPS
    # 14, sum  5
    # 15, sig  6
    # 17, moy  7
    # DELTA
    # 22, sum  8
    # 23, sig  9
    # 25, moy 10

    # Extracts the values to be easily usable
    ra = [i[0] for i in tab]    # ra
    dec = [i[1] for i in tab]   # dec
    sig = [i[3] for i in tab]   # standard deviation fits
    avgf = [i[4] for i in tab]  # mean fits
    avg = [i[10] for i in tab]  # mean delta

    # Plots the first histogram (avgf)
    G = gridspec.GridSpec(6, 3)
    axes_1 = plt.subplot(G[0, 0:2])
    plt.hist(avgf, 100, normed=1, alpha=0.75, orientation="vertical")

    # Plots avg = f(avgf)
    axes_2 = plt.subplot(G[1:3, 0:2], sharex=axes_1)
    plt.scatter(avgf, avg, alpha=0.50)
    plt.xlabel('avg FITS')
    plt.ylabel('delta avg')

    # Plots the second histogram (avg)
    axes_3 = plt.subplot(G[1:3, 2], sharey=axes_2)
    plt.hist(avg, 50, normed=1, alpha=0.75, orientation="horizontal")

    # Plots the mollwide projection of the values too high in absolute value
    axes_4 = plt.subplot(G[3:6, 3:6])
    plt.title("Mollweide Projection")
    m = Basemap(projection='moll', resolution='l', area_thresh=1000.0, lat_0=0, lon_0=0)

    # Draws the parallels and meridians with a 30° angle
    m.drawparallels(np.arange(-90., 120., 30.))
    m.drawmeridians(np.arange(0., 420., 60.))

    for i in range(0, len(tab)):
        x, y = m(ra[i], dec[i])
        color = get_marker_color(avg[i])
        if color != 'no':
            m.plot(x, y, get_marker_color(math.fabs(avg[i])), markersize=5, alpha=0.5)

    plt.show()
