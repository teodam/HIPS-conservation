import subprocess
import time


class Aladin:
    def __init__(self, path_to_java, path_to_jar, max_memory, gui):
        # Command to start aladin with gui
        self.start_cmd = "{} -Xmx{}m -jar {}".format(path_to_java, max_memory, path_to_jar)
        # Command to start aladin without gui
        self.start_cmd_nogui = "{} -Xmx{}m -jar {} -nogui".format(path_to_java, max_memory, path_to_jar)
        # Boolean set to set to false after the first measurement
        self.first_measure = True
        # Aladin process
        self.proc = None
        # Boolean given to set the state of the gui
        self.gui = gui

    def start(self):
        # This function starts aladin

        # With gui
        if self.gui:
            self.proc = subprocess.Popen(self.start_cmd,            # Command to execute
                                         shell=True,                # The command must be formatted the same way as it would be in shell
                                         stdin=subprocess.PIPE,     # Links the stdin to a pipe
                                         stdout=subprocess.PIPE,    # Links the stdout to a pipe
                                         universal_newlines=True)   # Enables the strings to be used for commands
                                                                    # Otherwise only bytes-like are permitted

            # Waits 10 seconds to be sure that aladin is completely started.
            # If not it will not recieve the firsts instructions
            time.sleep(10)
            # Skips the first 5 lines on stdout (corresponding to the aladin's introduction)
            for i in range(5):
                print(self.proc.stdout.readline())
        # Without gui
        else:
            self.proc = subprocess.Popen(self.start_cmd_nogui,
                                         shell=True,
                                         stdin=subprocess.PIPE,
                                         stdout=subprocess.PIPE,
                                         universal_newlines=True)

    def _skip(self):
        # Skips the lines we don't want to use during the process. If it's the first measure we keep two more lines,
        # it's the header of the columns
        if self.first_measure:
            if self.gui:
                for i in range(10):
                    self.proc.stdout.readline()
            else:
                # Skips the 2 lines order=-1 when the measures are made on the fits and hips images
                for i in range(2):
                    self.proc.stdout.readline()
        else:
            if self.gui:
                for i in range(12):
                    self.proc.stdout.readline()
            else:
                # Skips the 2 lines order=-1 when the measures are made on the fits and hips images
                # Skips also the line with the columns' names
                # the line with the --------- under the columns' names
                for i in range(4):
                    self.proc.stdout.readline()

    def communicate(self, cmd):
        # Sends a command to aladin. the command can be a string due to the Popen argument : universal_newlines = True
        self.proc.stdin.write(cmd)
        self.proc.stdin.flush()

    def communicate_hips(self, cmd):
        # Same as communicate but in gui mode, aladin returns a line on stdout when a hips is loaded so we skip it.
        # In no gui it doesn't return anything. Loading fits files doesn't return anything so you must use
        # communicate_hips only to load hips files
        self.communicate(cmd)
        if self.gui:
            self.proc.stdout.readline()

    def read_output(self):
        # Read the aladin's output
        # Skips the unwanted lines
        self._skip()
        res = []
        if self.first_measure:
            # For the first measure only, we save th 4 lines
            # the line with the columns' names
            # the line with the --------- under the columns' names
            # and the 2 lines measures in this order : fits then hips.
            for i in range(4):
                output = self.proc.stdout.readline()
                res.append(output)
        else:
            # If it's not the first measure, the two firsts lines have already benn skipped
            # We save the 2 lines measures in this order : fits then hips.
            for i in range(2):
                output = self.proc.stdout.readline()
                res.append(output)
        # Return a list of the saved lines
        return res

    def stop(self):
        # Send a stop command to aladin
        self.communicate("quit\n")
