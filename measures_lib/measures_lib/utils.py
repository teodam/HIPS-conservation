import math
import numpy as np


# NB here the spherical coordinates are set in the physical way (not mathematical)
# So Phi is on the horizontal plan and goes from 0 to 360°
# And Theta goes from the top of the sphere to the bottom (0 to 180°)
# In mathematics it's in the other way (phi is vertical [0, 180] and theta is horizontal [0, 360])


def polar2radec(theta, phi):
    """
    Converts polar coordinates (in degree) to RA/DEC coordinates (in rad)

    Args:
        phi: Phi angle in rad
        theta: Theta angle in rad

    Returns: The tuple of the final coordinates (RA, DEC)

    """
    ra = phi * 180 / np.pi
    dec = (np.pi / 2 - theta) * 180 / np.pi
    return ra, dec


def radec2polar(ra, dec):
    phi = ra * (np.pi / 180)
    theta = dec * (-np.pi / 180) + (np.pi / 2)
    return theta, phi


class LoadingBar:
    """
    Creates a loading bar.
    The timer begins when the object is created.
    You just need to use the next_step method each iteration

    Args:
        leniter: number of iterations
        timer: display timer or not (bool)
    """

    def __init__(self, len_iter):
        self.len_iter = len_iter
        self.bar_width = 77
        self.progress = 1

    def next_step(self):
        """
        Jumps to the next step and display an updated loading bar.

        """

        fraction = self.progress / self.len_iter
        progress = int(math.ceil(fraction * self.bar_width))

        print("".join(("\r[", "=" * progress, " " * (self.bar_width - progress), "] - ",
                       str(self.progress), "/", str(self.len_iter))), end="")

        self.progress += 1