import os

import numpy as np

from astropy.io import fits

from utils import LoadingBar


def modify_fits_in_directory_to_random(dir_path):
    """
    Replaces the data of all the fits files in the given directory with random generated values

    Args:
        dir_path: absolute path of the directory containing all the fits files

    """

    images = ["{}/{}".format(dir_path, f) for f in os.listdir(dir_path)]
    # Loading bar
    loading = LoadingBar(len(images))
    # For each file in the directory modify the picture
    # NB : this directory MUST contain EXCLUSIVELY the fits images to modify
    for image in images:
        loading.next_step()

        modify_fits_to_random(image)


def modify_fits_to_random(image_path):
    """
        Replaces the data of the given fits files with random generated values

        The generation is done in two steps:
        * the background is filled with a gaussian noise
        * some stars with some random flux ares added to this

        NB: the stars flux follows a gaussian distribution

        Args:
            image_path: absolute path of the fits file

        """

    # Opens the image
    hdu_list = fits.open(image_path, mode='update', ignore_blank=True)
    # Adds the gaussian noise in background
    hdu_list = generate_background_noise(hdu_list)
    # Adds the stars randomly generated
    hdu_list = generate_stars(hdu_list)
    # Override the original image
    hdu_list.flush()
    # Close the file
    hdu_list.close()

def generate_background_noise(hdu_list):
    """
    Generation of the gaussian noise to create a background of the picture

    Args:
        hdu_list: hdu_list of the opened fits file (cf doc astropy)

    Returns:
        The modified hdu_list

    """
    # Parameters of the gaussian noise
    sigma_background_gaussian_noise = 10
    mu_background_gaussian_noise = 1
    # Replace each pixel of the image with a gaussain noise
    for index in np.ndindex(hdu_list[0].header["NAXIS1"], hdu_list[0].header["NAXIS2"]):
        hdu_list[0].data[0][index[0]][index[1]] = np.random.normal(sigma_background_gaussian_noise,
                                                                   mu_background_gaussian_noise)
    return hdu_list


def generate_stars(hdu_list):
    """
    Generation of the random stars

    Args:
        hdu_list: hdu_list of the opened fits file (cf doc astropy)

    Returns:
        The modified hdu_list

    """
    # Parameters of the stars
    number_of_stars = 30
    radius_of_stars = 10

    # Size of the image
    x_size = hdu_list[0].header["NAXIS1"]
    y_size = hdu_list[0].header["NAXIS2"]

    # Generates for each stars random coordinates in the picture
    # The generation take into account the radius of the stars to avoid to generate stars with the center on
    # the edge of the picture
    for i in range(0, number_of_stars):

        x_pos = np.random.random_integers(radius_of_stars, x_size - 1 - radius_of_stars)
        y_pos = np.random.random_integers(radius_of_stars, y_size - 1 - radius_of_stars)

        hdu_list = generate_flux_star(hdu_list, x_pos, y_pos)

    return hdu_list


def generate_flux_star(hdu_list, x_pos, y_pos):
    """
    Generation of one star given its coordinates

    Args:
        hdu_list: hdu_list of the opened fits file (cf doc astropy)
        x_pos: star's x coordinate
        y_pos: star's y coordinate

    Returns:
        The modified hdu_list

    """
    # Parameters of the star
    min_flux = 200
    max_flux = 1000
    sigma_star = 1
    mu_star = 0
    radius_of_stars = 10

    # Generation of a random star flux in the given range [min_flux, max_flux]
    flux = np.random.random_integers(min_flux, max_flux)

    # Once we have generated the flux at the center of the star, we generate the flux around the star accordingly
    # to a gaussian law to simulate a true star.
    for x in range(x_pos - radius_of_stars, x_pos + radius_of_stars + 1):
        for y in range(y_pos - radius_of_stars, y_pos + radius_of_stars + 1):
            r = np.sqrt(np.power((x_pos - x), 2) + np.power((y_pos - y), 2))
            if r < radius_of_stars:
                hdu_list[0].data[0][y][x] += flux * gaussian(sigma_star, mu_star, r)

    return hdu_list


def gaussian(sigma, mu, x):
    """
    Gaussian law with the parameters sigma, mu and x

    Args:
        sigma: sigma parameter of the gaussian law
        mu: mu parameter of the gaussian law
        x: x parameter of the gaussian law

    Returns: The result of the gaussian law

    """
    return np.exp(-1 * (x - np.power(mu, 2)) / 2 * np.power(sigma, 2))/np.sqrt(2 * np.pi * np.power(sigma, 2))
