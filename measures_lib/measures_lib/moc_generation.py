import os

import pymocCustom
import healpy as hp
import numpy as np

from astropy.io import fits
from astropy import wcs

from . import hips
from . import utils

# The library used is pymocCustom. It's basically the pymoc library that I have modified for some performance issues.
# In this library i have disabled some verifications which are very very slow.
# The modify library and the original one are used the same way. So if you prefer you can use
# import pymoc
# instead of import pymocCustom
# NB : with the original library you will multiply the process time by 4

def gen_hips_moc_no_overlap(hips, general_order):
    """
    Generates a MOC of the healpix map where the originals pictures don't overlap each others.

    To do this, it adds to the final moc the moc cropped of each original fits file

    Creates a fits file of the moc

    Args:
        hips: Hips object
        general_order: Order of the MOC desired

    """
    # The moc is generated in the working directory
    path = '.'
    # Name of the moc
    moc_name = 'moc.fits'
    # Dictionary containing the generated moc to avoid recalculate them
    moc_dic = {}
    # Originals fits files
    fits_files = [f for f in os.listdir(hips.orig_path)]
    # Creation of the final moc (empty for now)
    final_moc = pymocCustom.MOC()

    loading = utils.LoadingBar(len(fits_files))
    for fits_file in fits_files:
        loading.next_step()
        # Add to the moc the cropped moc of the image (moc of the image without the overlays areas)
        final_moc += generate_pic_cropped_moc(fits_file, hips, general_order, moc_dic)

    # Loads the hips moc
    moc_hips = pymocCustom.MOC()
    moc_hips.read("{}/Moc.fits".format(hips.root_path))
    # We keep the intersection with the two mocs to exclude some areas without data (example : IRISB4H0)
    final_moc = moc_hips.intersection(final_moc)
    # Write the moc file
    final_moc.write("{}/{}".format(path, moc_name))


def generate_pic_cropped_moc(pic, hips, general_order, moc_dic):
    """
    Selects all the original fits files' names that surround the the given fits file
    Creates the cropped moc of the picture

    Args:
        pic: fits file name
        hips: Hips object
        general_order: MOC order
        moc_dic: dictionary containing the generated mocs

    Returns:
        moc of the central fits file pic cropped
    """
    # To get the cropped moc of a fits image, we start to find all the healpix tiles that intersects the fits image.
    tiles = find_tiles_matching_with_a_picture(hips, pic)
    # Then we find all the fits images that intersects the tiles and we remove from this list the starting fits image
    surroundings = find_pictures_matching_with_the_tiles(hips, pic, tiles)
    # We have now all the fits images that surround the starting fits image
    # We create all the mocs oh these images stored in moc_dic
    # with some mathematical operations we finally get the cropped moc of the starting image
    moc = gen_pic_moc_no_overlay(hips, pic, surroundings, general_order, moc_dic)

    return moc


def find_tiles_matching_with_a_picture(hips, pic):
    """
    For a given fits file name, finds all the healpix tiles that overlap it

    Args:
        pic: fits file name

    Returns:
        Iterator of the healpix tiles

    """
    return ((order, npix) for order, npix in hips.index if hips.index[order, npix].count(pic) != 0)


def find_pictures_matching_with_the_tiles(hips, pic, tiles):
    """
    for a given array/iterator of healpix tiles, finds all the original fits files that overlap them

    Args:
        pic: fits file name of the central picture
        tiles: array/iterator of healpix tiles

    Returns:
        Array of the original fits files' names found

    """
    results = []
    for order, tile in tiles:
        for result in hips.index[order, tile]:
            if (result not in results) and result != pic:
                results.append(result)

    # results.remove(pic)

    return results


def gen_pic_moc_no_overlay(hips, base_pic, surrounding_pics, general_order, moc_dic):
    """
    Creates a cropped moc of the base picture

    Args:
        hips: Hips object
        base_pic: central fits file name
        surrounding_pics: array of the surrounding fits files' names
        general_order: MOC order
        moc_dic: dictionary containing the generated mocs

    Returns:
        Cropped moc of the base picture

    """
    # Generation of the path to access the starting fits image
    base_name = "{}/{}".format(hips.orig_path, base_pic)
    # If the moc of this image is already in the dictionary we use it. If not we generate it.
    if (base_name, general_order) not in moc_dic:
        moc_dic[base_name, general_order] = gen_pic_moc(base_name, general_order)
    base = moc_dic[base_name, general_order]

    # Creation of a moc representing the surroundings of our starting fits image
    surrounding = pymocCustom.MOC()

    # For each picture of the surrounding, we test if the moc is already in the dictionary. If not we generate it.
    for pic in surrounding_pics:
        name = "{}/{}".format(hips.orig_path, pic)
        if (name, general_order) not in moc_dic:
            moc_dic[name, general_order] = gen_pic_moc(name, general_order)
        # Add the moc to the surroundings moc
        surrounding = surrounding + moc_dic[name, general_order]
    # He have now a moc of the starting image and an other moc of the surrounding images
    # |---|---|---|
    # | B | C | D |
    # |---|---|---|
    # | I | A | E |
    # |---|---|---|
    # | H | G | F |
    # |---|---|---|
    # So the moc base contains A and the moc surroundings contains B, C, D, E, F, G, H, I
    #
    # He have just to make the subtraction
    final = base - surrounding

    return final


def gen_pic_moc(pic_path, general_order):
    """
    Generates a moc from a fits image

    Args:
        pic_path: path of the picture
        general_order: MOC order

    Returns:
        Moc object
    """
    # Opens the fits image
    hdu_list = fits.open(pic_path, mode='update', ignore_blank=True)

    # Due to some bugs of the wcs library we have to create our wcs manualy.
    w = wcs.WCS(naxis=2)
    w.wcs.crpix = [hdu_list[0].header["CRPIX1"], hdu_list[0].header["CRPIX2"]]
    w.wcs.cdelt = [hdu_list[0].header["CDELT1"], hdu_list[0].header["CDELT2"]]
    w.wcs.crval = [hdu_list[0].header["CRVAL1"], hdu_list[0].header["CRVAL2"]]
    w.wcs.ctype = [hdu_list[0].header["CTYPE1"], hdu_list[0].header["CTYPE2"]]

    # Size of the picture
    naxis1 = hdu_list[0].header["NAXIS1"]
    naxis2 = hdu_list[0].header["NAXIS2"]

    # To optimize the process I had to optimize a lot this part

    # First i will use here numpy arrays
    # This will create an array containing 2 lines and a number of columns equals to the number of pixels of the image
    # All this array is filled with 0
    ra_dec = np.zeros((naxis1 * naxis2, 2), np.float_)

    # We'll start to fill the array
    # we have now something like this for a 500*500 image :
    # 0 0 0 0 0 0 0 0 0 0 0  0  ... 0
    # 0 1 2 3 4 5 6 7 8 9 10 11 ... 499
    ra_dec[:naxis2, 1] = np.array(range(naxis2))

    # Here we'll use the copy of memory of numpy which is optimized
    for i in range(1, naxis1):
        ra_dec[i*naxis2:(i+1)*naxis2, 0] = i
        ra_dec[i*naxis2:(i+1)*naxis2, 1] = ra_dec[:naxis2, 1]
    # We will obtain an array like this:
    # 0 0 0 0 ... 0   1 1 1 1 ... 1   2 2 2 2 ... 2   ...... 499
    # 0 1 2 3 ... 499 0 1 2 3 ... 499 0 1 2 3 ... 499 ...... 499

    # We have now an index of all the positions of all the pixels on the image
    # We'll convert all theses coordinates in the WCS system
    ra_dec_wcs = w.wcs_pix2world(ra_dec, 0)
    # Converts (RA, DEC) coordinates to polar ones
    theta, phi = utils.radec2polar(ra_dec_wcs[:, 0], ra_dec_wcs[:, 1])
    # With all these coordinates we can obtain all the corresponding healpix tiles at a given order
    healpix_tiles = hp.ang2pix(2**general_order, theta, phi, nest=True)
    # We finally generates the moc with these tiles
    moc = pymocCustom.MOC(general_order, healpix_tiles)

    return moc
