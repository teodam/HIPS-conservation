import os

import healpy as hp
import numpy as np

from pymocCustom import *
from pymocCustom.util.plot import plot_moc

from . import utils
from . import polygon
from . import aladin


def find_fit(RA, DEC, tab, order, hips):
    # Finds the fits image corresponding to a given couple of coordinates (RA, DEC)

    # We take one tile of the disc. It doesn't matter which one is chosen. Here, the number 0 is chosen.
    tile = tab[0]

    # Find the number of the tile at the max hips order
    # NB the moc order is higher than the max hips order
    for i in range(order, hips.max_order, -1):
        tile = int(np.floor(tile / 4))

    # Open the index of the tile
    f = open("{}/HpxFinder/Norder3/Dir0/Npix{}".format(hips.root_path, tile), 'r')

    # For each fits file inthis index, creates a polygon corresponding to the image and
    # check if the measure center is inside. If so, we have found the fits image for this measure
    for line in f.readlines():
        coords_string = line.split('"')[-2].split(" ")[2:]
        coords = [float(co) for co in coords_string]

        poly = polygon.Polygon(name=("{}/{}".format(hips.orig_path, line.split('"')[3])), edges=(
            polygon.Edge(
                a=polygon.Pt(x=coords[0], y=coords[1]),
                b=polygon.Pt(x=coords[2], y=coords[3])),
            polygon.Edge(
                a=polygon.Pt(x=coords[2], y=coords[3]),
                b=polygon.Pt(x=coords[4], y=coords[5])),
            polygon.Edge(
                a=polygon.Pt(x=coords[4], y=coords[5]),
                b=polygon.Pt(x=coords[6], y=coords[7])),
            polygon.Edge(
                a=polygon.Pt(x=coords[6], y=coords[7]),
                b=polygon.Pt(x=coords[0], y=coords[1]))))
        point = polygon.Pt(x=RA, y=DEC)

        if poly.is_point_inside(point):
            # returns the name of the file : my_image.FIT
            return "{}{}".format(poly.name, hips.extension)
    return None


def create_dictionary(hips, basemoc, order, order_measure, radius, plotmoc=False, createmoc=False):
    # Creates the dictionary containing the measures to do.
    # 1 measure at the center of each tile at the order order_measure

    # Opens the moc
    moc = MOC()
    moc.read(basemoc)

    # Initialize the result dictionnary
    result = {}
    heal_filtered = []

    # Loading bar
    progress = utils.LoadingBar(len(range(0, 12 * (4 ** order_measure))))

    # For each tile of the sphere at the measure order
    for tile in range(0, 12*(4**order_measure)):
        progress.next_step()

        # Find the theta phi coordinates of the center of the tile. It'll be the center of the measure
        theta, phi = hp.pix2ang(2 ** order_measure, tile, nest=True)

        # Converts (theta, phy) coordinates into (RA, DEC)
        RA, DEC = utils.polar2radec(theta, phi)

        # Gets the tiles at the moc order contained in a disc. It's the tiles in the measure
        heal_ids = hp.query_disc(2**order, hp.ang2vec(theta, phi), np.deg2rad(radius), nest=True)

        contained = True
        temp = []

        # For each tile found in the measure disc we check if the tile is inside the moc
        for heal_id in heal_ids:
            # If one tile is not contained in the moc, we don't have to do this measure
            if not moc.contains(order, int(heal_id)) or heal_ids == []:
                contained = False
                break
            else:
                # If it's contained, we put it into a temporary list and we remove the \n
                temp.append(int(str(heal_id).replace('\n', '')))

        # If it's contained we add the tiles to heal_filtered and we create an entry in the dictionnary
        if contained and heal_ids != []:
            heal_filtered += temp
            result.update({(RA, DEC, radius): find_fit(RA, DEC, temp, order, hips)})

    # Additional features :
    # createmoc creates a moc file with all the measures planned
    # plotmoc open a window and plot the moc on a mollwide projection
    # if plotmoc only is used, the moc file is deleted
    # NB : plotmoc freezes the program until the visualisation window is closed
    if createmoc and plotmoc:
        mesures = MOC(order, tuple(heal_filtered))
        mesures.write('mesures - {} - {} - {}.FIT'.format(order, order_measure, radius))
        plot_moc(mesures, projection='moll')
    elif createmoc:
        mesures = MOC(order, tuple(heal_filtered))
        mesures.write('mesures - {} - {} - {}.FIT'.format(order, order_measure, radius))
    elif plotmoc:
        mesures = MOC(order, tuple(heal_filtered))
        mesures.write('mesures - {} - {} - {}.FIT'.format(order, order_measure, radius))
        plot_moc(mesures, projection='moll')
        os.remove('mesures - {} - {} - {}.FIT'.format(order, order_measure, radius))
    print("")
    # returns the measure dictionnary
    return result


def generate_script(hips, dict, script_output='measures.script'):
    # Generates the aladin script using the previously generated dictionary

    # Opens the output script file
    f = open(script_output, 'w')

    # Loading bar
    loading = utils.LoadingBar(len(dict))

    # The first instruction of the script is the loading command for the hips
    print('load {}'.format(hips.root_path), flush=True, file=f)

    # Iterate on the dictionnary previously generated
    for RA, DEC, radius in dict:
        # Adds one step to the loading bar
        loading.next_step()
        # If the dictionnary contains the fits path for the measure
        if dict[(RA, DEC, radius)] is not None:

            # A measurement MUST have this structure in the script:
            #
            # rm Drawing                                    <= delete Drawing plan (even if there isn't one)
            # load path_to_fits_file/my_fits_file.fits      <= load the fits file
            # cview my_fits_file                            <= focus on the fits file
            # draw phot(value, value, value°, max)          <= measure
            # cview HIPS                                    <= focus on the fits file
            # draw phot(value, value, value°, max)          <= measure
            # rm my_fits_file                               <= delete the fits file
            # export Drawing /dev/stdout                    <= saves the values to /dev/stdout  NB : WORKS ONLY ON LINUX

            print('rm Drawing', flush=True, file=f)
            print('load {}'.format(dict[(RA, DEC, radius)]), flush=True, file=f)
            print('cview {}'.format(dict[(RA, DEC, radius)].split("/")[-1].split('.')[0]), flush=True, file=f)
            print('draw phot({}, {}, {}°, max)'.format(RA, DEC, radius), flush=True, file=f)
            print('cview HIPS', flush=True, file=f)
            print('draw phot({}, {}, {}°, max)'.format(RA, DEC, radius), flush=True, file=f)
            print('rm {}'.format(dict[(RA, DEC, radius)].split("/")[-1].split('.')[0]), flush=True, file=f)
            print('export Drawing /dev/stdout', flush=True, file=f)
    # Close the script file
    f.close()
    print("")


def refactor_data(tab, out):
    # Refactor the data contained in a list

    # Loading bar
    loading = utils.LoadingBar(len(range(int(len(tab)/2))))

    # Opens the output data file
    final = open(out, 'w')

    # Gets the two firsts line of the list
    # Those two lines correspond to the column's titles and the -------- under them
    line0 = tab.pop(0)
    line1 = tab.pop(0)

    # Duplicate and format the titles
    line0 = line0[:-1].split("\t")
    line0 = line0[:2] + line0[4:6] + [line0[3]] + ["{} - FIT".format(i) for i in line0[6:]] + ["{} - HIPS".format(i) for i in line0[6:]] + ["{} - DELTA".format(i) for i in line0[6:]]
    line0 = "\t".join(line0)

    # Duplicates the -------
    line1 = line1[:-1].split("\t")
    line1 = line1[:2] + line1[4:6] + [line1[3]] + (line1[6:] * 3)
    line1 = "\t".join(line1)

    # Add the two lines in the file
    print(line0, file=final)
    print(line1, file=final)

    # Reads the list two lines a time
    for i in range(int(len(tab)/2)):
        loading.next_step()
        # The first line is always the fits measure
        fits_line = tab.pop(0)[:-1].split("\t")
        # The second line is always the hips measure
        hips_line = tab.pop(0)[:-1].split("\t")

        # Check if the coordinates are the same between the fits measure and the hips one
        is_ra_equals = float(hips_line[0]) == float(fits_line[0])
        is_dec_equals = float(hips_line[1]) == float(fits_line[1])

        # If the coordinates are equal
        if is_ra_equals and is_dec_equals:
            # Creates the line containing fits and hips values
            final_line = fits_line[:2] + fits_line[4:6] + [fits_line[3]] + fits_line[6:] + hips_line[6:]

            # Appends to this line the calculated errors in %
            for j in range(6, len(hips_line)):
                try:
                    final_line.append(str((float(hips_line[j]) - float(fits_line[j])) * 100 / float(fits_line[j])))
                except Exception as e:
                    print("j = ", j)
                    print("title = ", line0.split('\t')[j])
                    print("hips_line = ", hips_line)
                    print(hips_line[j])
                    print("fits_line = ", fits_line)
                    print(fits_line[j])
                    raise e
            # Adds the complete line to the file
            print("\t".join(final_line), file=final)
    print("")
    # Closes the file
    final.close()


def check_measure(measures):
    # check for NanN or '' values after the measure
    # return True if the measures are valid, False otherwise
    for measure in measures:
        if 'NaN' in measure or ' ' in measure.split('\t'):
            return False
    return True


def execute_aladin(script, gui=False, check=False):
    # Executes the script with Aladin

    # Initialize the result list
    result = []
    # Creation of the Aladin object
    al = aladin.Aladin("/home/teodori/jre1.8.0_91/bin/java", "/home/teodori/Aladin/AladinDamien.jar", 4096, gui)
    # Starts Aladin
    al.start()
    # Opens the script file
    fin = open(script, 'r')
    # Gets the number of iterations
    iterations = int((len(fin.readlines()) - 1) / 8)
    # Set the cursor of the file at the beginning
    fin.seek(0)

    # First we load the hips in aladin
    load_hips = fin.readline()
    al.communicate_hips(load_hips)

    # Loading bar
    loading = utils.LoadingBar(len(range(iterations)))

    # For each measurement = 1 measure on a fits file and on the hips survey
    for i in range(iterations):
        loading.next_step()

        # Boolean of the validity of the measures set to False
        valid = False

        # Initialize attempts counter
        attempt = 0

        # Saves the actual position of the file's cursor
        last_pos = fin.tell()

        # At each measure, we check its validity, if it's not valid the measure is done another time.
        # We ignore the measure after 3 attempts
        while not valid and attempt < 3:
            # Sends all the commands to aladin to perform the measure
            for j in range(8):
                line = fin.readline()
                al.communicate(line)
            # Read the output
            temp = al.read_output()
            # Check the validity if the check parameter is True
            valid = check_measure(temp) if check else True
            # Count 1 attempt
            attempt += 1

            if valid:
                # If the measure is valid the first measure is set to False because all others won't be the first
                al.first_measure = False
                # Appends the measures to the result
                result += temp
            elif attempt < 3:
                # If the measure is not valid and wa haven't reached the third attempt, set the file's cursor the the
                # last saved position in order to perform again the measurement.
                fin.seek(last_pos)
    print("")
    # Stop aladin
    al.stop()
    # Delete the object
    del al
    # Return the measures list
    return result
