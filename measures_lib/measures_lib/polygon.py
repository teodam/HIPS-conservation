import sys
from collections import namedtuple

Pt = namedtuple('Pt', 'x, y')  # Point
Edge = namedtuple('Edge', 'a, b')  # Polygon edge from a to b


class Polygon:
    """
    Creates a polygon

    Example:
        ::
            polygon = Polygon(name="my polygon", edges=(
                Edge(a=Pt(x=0, y=0), b=Pt(x=0, y=10)),
                Edge(a=Pt(x=0, y=10), b=Pt(x=10, y=10)),
                Edge(a=Pt(x=10, y=10), b=Pt(x=10, y=0)),
                Edge(a=Pt(x=10, y=0), b=Pt(x=0, y=0))))

    Args:
        name: name of the polygon
        edges: tuple of Edge
    """

    def __init__(self, name, edges):

        self._eps = 0.00001
        self._huge = sys.float_info.max
        self._tiny = sys.float_info.min
        self.name = name
        self.edges = edges

    def rayintersect_seg(self, p, edge):
        """
        Takes a point p=Pt() and an edge of two endpoints a,b=Pt() of a line segment returns boolean

        This algorithm don't work in all the cases. For example if we ares at the limit where the coordinates goes from
        360 to 0 it won't consider a small polygon but a polygon that goes all around the sphere.

        But since this zone is generally ingored in the moc it won't be a big issue.
        I didn't had the time to find something that works perfectly in this case

        Args:
            p: point proc=Pt()
            edge: edge of two endpoints a,b=Pt()

        Returns:
            bool

        """
        a, b = edge
        if a.y > b.y:
            a, b = b, a
        if p.y == a.y or p.y == b.y:
            p = Pt(p.x, p.y + self._eps)

        if (p.y > b.y or p.y < a.y) or (p.x > max(a.x, b.x)):
            return False

        if p.x < min(a.x, b.x):
            intersect = True
        else:
            if abs(a.x - b.x) > self._tiny:
                m_red = (b.y - a.y) / float(b.x - a.x)
            else:
                m_red = self._huge
            if abs(a.x - p.x) > self._tiny:
                m_blue = (p.y - a.y) / float(p.x - a.x)
            else:
                m_blue = self._huge
            intersect = m_blue >= m_red
        return intersect

    def is_point_inside(self, p):
        # Check if a point is inside a polygon
        return self._odd(sum(self.rayintersect_seg(p, edge) for edge in self.edges))

    def polyp_print(self):
        print("\n  Polygon(name='%s', edges=(" % self.name)
        print('   ', ',\n    '.join(str(e) for e in self.edges) + '\n    ))')

    @staticmethod
    def _odd(x): return x % 2 == 1