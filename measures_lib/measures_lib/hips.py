import glob
import re


class Hips:
    """
    Creates a HIPS object

    **Attributes**

    The object has 4 attributes:
        * root_path: absolute path to the HIPS root directory.
        * orig_path: absolute path to the directory containing the original fits files which have served to create the HIPS.
        * extension: extension of the fits files in the orig_path. (.fit .fits .FIT .FITS)
        * index: explained below

    **Index**

    The index attribute is a dictionnary containing the data of the HpxFinder directory.
    The key of this dictionnary is a tuple (order, name_of_the_healpix_index)
    The value is an array containing the name of all the original pictures that intersect the healpix tile

    NB: the name of the healpix index of a tile is the same as the tile's name

    Example:
        ::

            {(3, 'Npix689'): ['I084B1H0.FIT', ...], (3, 'Npix36'): ['I307B1H0.FIT', ...], ...}

    The Hips object is iterable.
    The iteration will be made on the indexes in the HpxFinder directory.
    It returns for each index file : (order, tile_index_directory_name, tile_index_name)

    Example:
        With a HIPS generated at the 6th order, Npix15200 will be (6, 'Dir10000', 'Npix15200')

    Args:
        root_path: Absolute path to the HIPS root directory
        orig_path: Absolute path to the directory containing the original FITS images
    """

    def __init__(self, root_path, orig_path):
        self.root_path = root_path
        self.orig_path = orig_path
        self.extension = self._get_extension()
        self.index = self.load_index()
        self.max_order = self._get_max_order()

    def __iter__(self):
        """
        Makes the Hips object iterable

        Returns:
            Tuple (order, tile_index_directory_name, tile_index_name)

        """
        return self._generator()

    def _generator(self):
        """
        Iterator on HpxFinder

        Returns:
            Tuple (order, tile_index_directory_name, tile_index_name)

        """

        order_directories = glob.glob("{}/HpxFinder/Norder[0-9]*".format(self.root_path))
        for order_directory in order_directories:

            tiles_directories = glob.glob("{}/Dir*".format(order_directory, 'HpxFinder'))
            for tiles_directory in tiles_directories:

                tiles = glob.glob("{}/Npix*".format(tiles_directory, 'HpxFinder'))
                for tile in tiles:

                    yield _get_tile_information(tile)

    def _get_extension(self):
        """
        Gets the extension of the original fits files

        Allowed extensions .fit .FIT .fits .FIT
        But only 1 is allowed in the original file directory

        Examples:
            * It's ok if the directory contains :

                * img1.fits
                * img2.fits
                * img3.fits

            * It's ok if the directory contains:

                * img1.FIT
                * img2.FIT
                * img3.FIT

            * It will raise an error if the directory contains:

                * img1.FIT
                * img2.FIT
                * img3.fits

        Returns:
            The extension

        """
        extensions = ['.fit', '.fits', '.FIT', '.FITS']

        result = [extension for extension in extensions if glob.glob("{}/*{}".format(self.orig_path, extension)) != []]

        if len(result) != 1:
            raise Exception('Too many different fits extensions')

        result = result[0]

        return result

    def _get_max_order(self):
        directories = glob.glob("{}/HpxFinder/Norder[0-9]*".format(self.root_path))
        max_order = int(re.search("Norder(\d+)", directories[0]).group(1))
        return max_order

    def load_index(self):
        """
        Creates a dictionnary

        The key is a tuple (order, name_of_the_healpix_tile) eg. (3, 'Npix61')
        The value is an array containing the names of the FITS images that intersects the tile

        Returns:
            the dictionnary

        """
        index = {(order, name): self._images_in_a_tile(order, directory, name) for order, directory, name in self}

        return index


    def _images_in_a_tile(self, order, directory, name):
        """
        Finds the images that intersects the tile

        Args:
            order: order in the HIPS survey
            directory: directory containing the tiles eg. Dir0
            name: name of the healpix tile

        Returns:
            Array containing these images

        """
        file = open(self.root_path + '/HpxFinder/Norder{}/{}/{}'.format(order, directory, name), 'r')
        result = ["{}{}".format(line.split('"')[3], self.extension) for line in file.readlines()]
        file.close()
        return result


def _get_tile_information(path):
    """
    Extracts from the path of the tile :
        - the tile name
        - the tile directory name
        - the order

    Args:
        path: Absolute path of the tile

    Returns:
        Tuple (order, tile_index_directory_name, tile_index_name)

    """
    order = int(re.search('Norder(\d+)', path).group(1))
    tile_index_directory_name = re.search('(Dir\d+)', path).group(1)
    tile_index_name = re.search('(Npix\d+)', path).group(1)

    return order, tile_index_directory_name, tile_index_name
