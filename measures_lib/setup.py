from setuptools import setup, find_packages

import measures_lib

setup(
    name='measures_lib',
    version=measures_lib.__version__,
    packages=find_packages(),
    author="Damien Teodori",
    author_email="",
    description="Performs measures to evaluate photometric conservation in HiPS processing",
    long_description=open('README.md').read(),
 
    # Ex: ["gunicorn", "docutils >= 0.3", "lxml==0.5a7"]
    # install_requires= ,

    include_package_data=True,
    url='',
    classifiers=[
        "Programming Language :: Python",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3.5",
    ],
)
