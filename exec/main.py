import timeit
from measures_lib import hips, moc_generation, measures, display
root_path = '/home/teodori/Bureau/Stage/Donnees/IRIS_B4H0/HIPS'
orig_path = '/home/teodori/Bureau/Stage/Donnees/IRIS_B4H0/FITS'

hips = hips.Hips(root_path, orig_path)

start = timeit.default_timer()

print("\nMOC generation:")
moc_generation.gen_hips_moc_no_overlap(hips, 10)
stop_moc = timeit.default_timer()
print("Time MOC creation = {} seconds".format(stop_moc - start))

print("\nDictionnary creation:")
#dictionary = measures.create_dictionary(hips, 'moc.fits', order=10, order_measure=5, radius=0.8, plotmoc=False, createmoc=False)
stop_dic = timeit.default_timer()
print("Time dic creation = {} seconds".format(stop_dic - stop_moc))

print("\nScript creation:")
#measures.generate_script(hips, dict, script_output='measures.script')
stop_script = timeit.default_timer()
print("Time script creation = {} seconds".format(stop_script - stop_dic))

print("\nMeasures:")
#tab = measures.execute_aladin('measures.script', gui=False, check=True)
stop_measures = timeit.default_timer()
print("Time measures = {} seconds".format(stop_measures - stop_script))

print("\nData refactoring:")
#measures.refactor_data(tab, 'measures.data')
stop_refactor = timeit.default_timer()
print("Time measures = {} seconds".format(stop_refactor - stop_measures))

print("Total time = {}".format(stop_refactor - start))

print("\nPlotting...")
display.plot('measures.data')
print("Done")